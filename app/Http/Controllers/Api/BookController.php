<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;
use Illuminate\Support\Facades\Validator;
use Throwable;

class BookController extends Controller
{
    public function index($id = null)
    {
        if ($id !== null) {
            $book = Book::find($id);
            if (!$book)
                return response()->json(['status' => false, 'message' => 'Book not found'], 500);
        } else {
            $book = Book::orderBy('id', 'DESC')->paginate(10);
        }
        return response()->json(['status' => true, 'result' => $book], 200);
    }

    public function store(Request $request)
    {
        Validator::extend('without_spaces', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });

        $validator = Validator::make($request->all(), [
            'book_code' => 'bail|required|unique:books,book_code,NULL,id,deleted_at,NULL|max:10|without_spaces',
            'title'  => 'required|max:50',
            'year'   => 'required|date_format:Y',
            'author' => 'required|max:30',
            'stock'  => 'bail|required|numeric|digits_between:1,5',
        ],[
            'without_spaces' => 'Whitespace not allowed.'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()], 400);
        }

        try {
            $book = Book::create([
                'book_code' => $request->book_code,
                'title'  => $request->title,
                'year'   => $request->year,
                'author' => $request->author,
                'stock'  => $request->stock,
            ]);

            return response()->json(['status' => true, 'message' => 'Book has been saved', 'result' => $book], 201);
        } catch (Throwable $ex) {
            return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
        }
    }

    public function update($id = null, Request $request)
    {
        $book = Book::find($id);

        if (!$book) {
            return response()->json(['status' => false, 'message' => 'Book not found'], 500);
        }

        Validator::extend('without_spaces', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });

        $validator = Validator::make($request->all(), [
            'book_code' => 'bail|required|unique:books,book_code,' . $id . ',id,deleted_at,NULL|max:10|without_spaces',
            'title'  => 'required|max:50',
            'year'   => 'required|date_format:Y',
            'author' => 'required|max:30',
            'stock'  => 'bail|required|numeric|digits_between:1,5',
        ], [
            'without_spaces' => 'Whitespace not allowed.'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()], 400);
        }

        try {
            $book->book_code = $request->book_code;
            $book->title = $request->title;
            $book->year = $request->year;
            $book->author = $request->author;
            $book->stock = $request->stock;
            $book->save();

            return response()->json(['status' => true, 'message' => 'Book has been updated', 'result' => $book], 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        $book = Book::find($id);

        if (!$book) {
            return response()->json(['status' => false, 'message' => 'Book not found'], 500);
        }

        try {
            $book->delete();
            return response()->json(['status' => true, 'message' => 'Book has been deleted', 'result' => $book], 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
        }
    }
}
