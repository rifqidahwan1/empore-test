<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;


class BookRequestController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $book = Book::orderBy('id', 'DESC')->get();
            return Datatables::of($book)->addIndexColumn()
                ->addColumn('action', function ($book) {
                    return '<a onclick="selectBook('.$book->id.')" Title="Choose this book" href="javascript:void(0)" style="color:green;" class="text-green-700 font-bold">Choose</a>';
                })
                ->make(true);
        }
        return view('member.request.request');
    }
}
