<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;
use App\Models\transaction;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class TransactionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $transaction = transaction::with('user', 'book')->orderBy('id', 'DESC')->get();
            return Datatables::of($transaction)->addIndexColumn()
                ->addColumn('action', function ($transaction) {
                    if ($transaction->status == 'REQUEST') {
                        return '<a onclick="updateTransaction(' . $transaction->id . ', `APPROVE`)" Title="Approve this transaction" href="javascript:void(0)" style="color:green;" class="mr-2 text-green-700 font-bold">APPROVE</a> <a Title="Reject this transaction" onclick="updateTransaction(' . $transaction->id . ', `REJECT`)" href="javascript:void(0)" style="color:red;" class="text-red-700 font-bold">REJECT</a>';
                    }
                    if ($transaction->status == 'APPROVE') {
                        return '<a onclick="updateTransaction(' . $transaction->id . ', `COMPLETE`)" Title="Complete this transaction" href="javascript:void(0)" style="color:blue;" class="text-blue-700 font-bold">COMPLETE</a>';
                    }
                })
                ->make(true);
        }

        return view('admin.transaction.transaction');
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()], 400);
        }

        $transaction = Transaction::find($request->id);

        if ($transaction) {
            if ($this->checkValidationStatus($transaction->status, $request->status)) {
                if ($request->status == 'APPROVE') {
                    if ($transaction->book->stock < 1) {
                        return response()->json(['status' => false, 'message' => 'Stok Buku Habis'], 500);
                    }
                    $transaction->book->stock = $transaction->book->stock - 1;
                } else if ($request->status == 'COMPLETE') {
                    $transaction->book->stock = $transaction->book->stock + 1;
                }
                try {
                    DB::beginTransaction();
                    $transaction->status = $request->status;
                    $transaction->save();
                    $transaction->book->save();
                    DB::commit();
                    return response()->json(['status' => true, 'result' => $transaction], 200);
                } catch (\Illuminate\Database\QueryException $ex) {
                    DB::rollback();
                    return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
                }
            }
        } else {
            return response()->json(['status' => false, 'message' => 'Transaction Not Found'], 400);
        }
    }

    private function checkValidationStatus($curr_status, $new_status)
    {
        $isValid = false;
        if ($curr_status == 'REQUEST') {
            if ($new_status == 'APPROVE' || $new_status == 'REJECT') {
                $isValid = true;
            }
        } else if ($curr_status == 'APPROVE') {
            if ($new_status == 'COMPLETE') {
                $isValid = true;
            }
        }
        return $isValid;
    }
}
