<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Validator;
use Throwable;


class MemberController extends Controller
{
    public function index(Request $request, $id = null)
    {
        if ($request->ajax()) {

            if($id){
                $user = User::find($id);
                return response()->json(['status' => true, 'result' => $user], 200);
            }
            
            $user = User::orderBy('id', 'DESC')->where(['rules' => 'member'])->get();
            return Datatables::of($user)->addIndexColumn()
                ->addColumn('action', function ($user) {
                    return '<a onclick="editUser(' . $user->id . ')" Title="Edit" href="javascript:void(0)" style="color:green;" class="text-green-700 font-bold">Edit</a> <a Title="Delete" href="javascript:void(0)" onclick="deleteUser(\'' . $user->id . '\')" style="color:red;" class="text-red-700 font-bold">Delete</a> </a>';
                })
                ->make(true);
        }
        return view('admin.member.member');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['bail', 'required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['bail', 'required', 'confirmed', Rules\Password::defaults()],
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()], 400);
        }

        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'rules' => 'member'
            ]);

            return response()->json(['status' => true, 'message' => 'User has been created', 'result' => $user], 201);
        } catch (Throwable $ex) {
            return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
        }
    }

    public function update($id = null, Request $request)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(['status' => false, 'message' => 'User not found'], 500);
        }

        if ($request->password == '' && $request->password_confirmation == '') {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['bail', 'required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['bail', 'required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
                'password' => ['bail', 'required', 'confirmed', Rules\Password::defaults()],
            ]);
        }

        //check if validation fails
        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()], 400);
        }

        try {
            $user->name = $request->name;
            $user->email = $request->email;
        
            if($request->password != '' && $request->password_confirmation != ''){
                $user->password = Hash::make($request->password);
            }

            $user->save();

            return response()->json(['status' => true, 'message' => 'User has been updated', 'result' => $user], 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
        }
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(['status' => false, 'message' => 'User not found'], 500);
        }

        try {
            $user->delete();
            return response()->json(['status' => true, 'message' => 'User has been deleted', 'result' => $user], 200);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
        }
    }
}
