<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;


class BookController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $book = Book::orderBy('id', 'DESC')->get();
            return Datatables::of($book)->addIndexColumn()
                ->addColumn('action', function ($book) {
                    return '<a onclick="editBook('.$book->id.')" Title="Edit" href="javascript:void(0)" style="color:green;" class="mr-2 text-green-700 font-bold">Edit</a> <a Title="Delete" href="javascript:void(0)" onclick="deleteBook(\'' . $book->id . '\')" style="color:red;" class="text-red-700 font-bold">Delete</a> </a>';
                })
                ->make(true);
        }
        return view('admin.book.book');
    }
}
