<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\Datatables;
use App\Models\transaction;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class MemberTransactionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $transaction = transaction::with('user', 'book')->where(['user_id' => Auth::user()->id])->orderBy('id', 'DESC')->get();
            return Datatables::of($transaction)->addIndexColumn()
               
                ->make(true);
        }

        return view('admin.transaction.transaction');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'errors' => $validator->errors()], 400);
        }

        $transaction = new Transaction;
        $transaction->user_id =  Auth::user()->id;
        $transaction->book_id = $request->id;
        $transaction->status = 'REQUEST';


        try {
            $transaction->save();
            return response()->json(['status' => true, 'result' => $transaction], 201);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['status' => false, 'message' => $ex->getMessage()], 500);
        }
    }

}
