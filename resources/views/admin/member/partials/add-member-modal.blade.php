<div class="hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center"
id="modal-add-member">
<div class="relative w-11/12 my-6 mx-auto max-w-3xl">
    <!--content-->
    <div
        class="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
        <!--header-->
        <div
            class="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
            <h2 class="text-lg font-medium text-gray-900">
                {{ __('Add New Member') }}
            </h2>
        </div>
        <!--body-->
        <div class="relative p-6 flex-auto">
            @include('admin.member.partials.add-member-form')
        </div>
        <!--footer-->
        <div
            class="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
            <button
                class="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button" onclick="toggleModal('modal-add-member')">
                Cancel
            </button>
            <x-primary-button form="form-add-member" id="btn-add-member" type="submit" value="Submit">
                {{ __('Submit') }}
            </x-primary-button>
        </div>
    </div>
</div>
</div>
<div class="hidden opacity-25 fixed inset-0 z-40 bg-black" id="modal-add-member-backdrop"></div>