<section>
    <form id="form-add-member" class="mt-6 space-y-6">
        @csrf
        @method('post')
        <div>
            <x-input-label for="name" :value="__('Name')" />
            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" autofocus autocomplete="name" />
            <x-input-error-form id="error_name" class="mt-1"/>
        </div>
        <div>
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input id="email" name="email" type="text" class="mt-1 block w-full" autocomplete="email" />
            <x-input-error-form id="error_email" class="mt-1"/>
        </div>

        <div>
            <x-input-label for="password" :value="__('Password')" />
            <x-text-input id="password" name="password" type="password" class="mt-1 block w-full" autocomplete="password" />
            <x-input-error-form id="error_password" class="mt-1"/>
        </div>

        <div>
            <x-input-label for="password_confirmation" :value="__('Confirm Password')" />
            <x-text-input id="password_confirmation" name="password_confirmation" type="password" class="mt-1 block w-full" autocomplete="password_confirmation" />
            <x-input-error-form id="error_password_confirmation" class="mt-1"/>
        </div>
    </form>
</section>


