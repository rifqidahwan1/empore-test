<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Lists of Transactions') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <table class="table table-striped table-bordered dt-responsive nowrap text-sm"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>User</th>
                            <th>Book Code</th>
                            <th>Book Title</th>
                            <th>Status</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    @section('script')
        <script type="module">
            // load datatable
            $(function() {
                var table = $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('transaction.index') }}",
                    pageLength: 10,
                    responsive: true,

                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                        },
                        {
                            data: 'id',
                            name: 'id',
                            visible: false
                        },
                        {
                            data: 'user.name',
                            name: 'user.name',
                            sortable: false
                        },
                        {
                            data: 'book.book_code',
                            name: 'book.book_code',
                            sortable: false
                        },
                        {
                            data: 'book.title',
                            name: 'book.title',
                            sortable: false
                        },
                        {
                            data: 'status',
                            name: 'status',
                            sortable: false
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ]
                });
            });
        </script>

        <script>
            // Update Transaction
            function updateTransaction(id, status) {
                var r = confirm(status + " THIS TRANSACTION?");
                if (r == true) {

                    let data = {
                        id: id,
                        status: status
                    }
                    let formData = new FormData();
                    for (let key in data) {
                        formData.append(key, data[key]);
                    }

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: "POST",
                        url: "{{ route('transaction.update') }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {

                        },
                        complete: function(data) {
                            $('#table').DataTable().ajax.reload();
                        },
                        error: function(data) {
                           if(Object.hasOwn(data.responseJSON, 'message')){
                            alert(data.responseJSON.message);
                           }
                        }

                    });
                }
            }
        </script>
    @endsection
</x-app-layout>
