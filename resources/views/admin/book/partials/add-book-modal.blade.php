<div class="hidden overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center"
id="modal-add-book">
<div class="relative w-11/12 my-6 mx-auto max-w-3xl">
    <!--content-->
    <div
        class="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
        <!--header-->
        <div
            class="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
            <h2 class="text-lg font-medium text-gray-900">
                {{ __('Add New Book') }}
            </h2>
        </div>
        <!--body-->
        <div class="relative p-6 flex-auto">
            @include('admin.book.partials.add-book-form')
        </div>
        <!--footer-->
        <div
            class="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
            <button
                class="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button" onclick="toggleModal('modal-add-book')">
                Cancel
            </button>
            <x-primary-button form="form-add-book" id="btn-add-book" type="submit" value="Submit">
                {{ __('Submit') }}
            </x-primary-button>
        </div>
    </div>
</div>
</div>
<div class="hidden opacity-25 fixed inset-0 z-40 bg-black" id="modal-add-book-backdrop"></div>