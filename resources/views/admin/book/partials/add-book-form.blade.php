<section>
    <form id="form-add-book" class="mt-6 space-y-6">
        @csrf
        @method('post')
        <div>
            <x-input-label for="book_code" :value="__('Book Code')" />
            <x-text-input id="book_code" name="book_code" type="text" class="mt-1 block w-full" autofocus autocomplete="book_code" />
            <x-input-error-form id="error_book_code" class="mt-1"/>
        </div>

        <div>
            <x-input-label for="title" :value="__('Title')" />
            <x-text-input id="title" name="title" type="text" class="mt-1 block w-full" autocomplete="title" />
            <x-input-error-form id="error_title" class="mt-1"/>
        </div>

        <div>
            <x-input-label for="year" :value="__('Year')" />
            <x-text-input id="year" name="year" type="text" class="mt-1 block w-full" autocomplete="year" />
            <x-input-error-form id="error_year" class="mt-1"/>
        </div>

        <div>
            <x-input-label for="author" :value="__('Author')" />
            <x-text-input id="author" name="author" type="text" class="mt-1 block w-full" autocomplete="author" />
            <x-input-error-form id="error_author" class="mt-1"/>
        </div>

        <div>
            <x-input-label for="stock" :value="__('Stock')" />
            <x-text-input id="stock" name="stock" type="number" class="mt-1 block w-full" autocomplete="stock" />
            <x-input-error-form id="error_stock" class="mt-1"/>
        </div>
    </form>
</section>
