<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Lists of Books') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">

                <div class="mb-6">
                    <x-primary-button onclick="toggleModal('modal-add-book')" type="button">
                        {{ __('Add New Book') }}
                    </x-primary-button>
                </div>

                <table class="strip hover nowrap text-sm" style="" id="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>Book Code</th>
                            <th>Title</th>
                            <th>Year</th>
                            <th>Author</th>
                            <th>Stock</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>

    @include('admin.book.partials.add-book-modal')

    @include('admin.book.partials.edit-book-modal')

    @section('script')
        <script type="module">
            // load datatable
            $(function() {
                var table = $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('book.index') }}",
                    pageLength: 10,

                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            sortable: false
                        },
                        {
                            data: 'id',
                            name: 'id',
                            visible: false
                        },
                        {
                            data: 'book_code',
                            name: 'book_code'
                        },
                        {
                            data: 'title',
                            name: 'title'
                        },
                        {
                            data: 'year',
                            name: 'year'
                        },
                        {
                            data: 'author',
                            name: 'author'
                        },
                        {
                            data: 'stock',
                            name: 'stock'
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ],
                    columnDefs: [{
                        targets: 6,
                        render: $.fn.dataTable.render.number('.', '.', 0, '')
                    }],
                });
            });

            // form add book
            $('#form-add-book').submit(function(e) {

                e.preventDefault();

                let form = $(this);
                let formData = new FormData(this);
                let button = $('#btn-add-book');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "{{ url('api/books') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,

                    beforeSend: function() {
                        button.prop('disabled', true);
                        button.html(`
                            <svg aria-hidden="true" role="status"
                                class="inline w-4 h-4 text-white animate-spin" viewBox="0 0 100 101"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                                    fill="#E5E7EB" />
                                <path
                                    d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                    fill="currentColor" />
                            </svg>
                        `);
                        $('.error-msg').html('');
                    },
                    success: function(data) {
                        setTimeout(function() {
                            $('#table').DataTable().ajax.reload();
                            toggleModal('modal-add-book');
                            form.trigger("reset");
                        }, 1000);
                    },
                    error: function(data) {
                        setTimeout(function() {
                            if (!$.isEmptyObject(data.responseJSON.errors)) {
                                $.each(data.responseJSON.errors, function(key, value) {
                                    let errorText = form.find('#error_' + key);
                                    errorText.text(value);
                                });
                            }
                        }, 1000);
                    },
                    complete: function(data) {
                        setTimeout(function() {
                            button.html(button.val());
                            button.prop('disabled', false);
                        }, 1000);
                    }
                });
            });

            // form edit book
            $('#form-edit-book').submit(function(e) {
                e.preventDefault();
                let form = $(this);
                let id = form.find('#id').val();
                let formData = new FormData(this);
                let button = $('#btn-edit-book');

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
             
                $.ajax({
                    type: "POST",
                    url: "{{ url('api/books') }}"+ "/" + id,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,

                    beforeSend: function() {
                        button.prop('disabled', true);
                        button.html(`
                            <svg aria-hidden="true" role="status"
                                class="inline w-4 h-4 text-white animate-spin" viewBox="0 0 100 101"
                                fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
                                    fill="#E5E7EB" />
                                <path
                                    d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
                                    fill="currentColor" />
                            </svg>
                        `);
                        $('.error-msg').html('');
                    },
                    success: function(data) {
                        setTimeout(function() {
                            $('#table').DataTable().ajax.reload();
                            toggleModal('modal-edit-book');
                            form.trigger("reset");
                        }, 1000);
                    },
                    error: function(data) {
                        setTimeout(function() {
                            if (!$.isEmptyObject(data.responseJSON.errors)) {
                                $.each(data.responseJSON.errors, function(key, value) {
                                    let errorText = form.find('#error_' + key);
                                    errorText.text(value);
                                });
                            }
                        }, 1000);
                    },
                    complete: function(data) {
                        setTimeout(function() {
                            button.html(button.val());
                            button.prop('disabled', false);
                        }, 1000);
                    }
                });
            });
        </script>

        <script>
            // delete book
            function deleteBook(id) {
                var r = confirm("Delete Book?");
                if (r == true) {
                    $.ajax({
                        type: "POST",
                        url: "{{ url('api/books') }}" + "/" + id,
                        data: {
                            _method: "DELETE"
                        },
                        success: function(data) {
                            $('#table').DataTable().ajax.reload();
                        }
                    });
                }
            }

            // edit book
            function editBook(id) {
                let form = $('#form-edit-book');
                $.ajax({
                    type: "GET",
                    url: "{{ url('api/books') }}" + "/" + id,

                    beforeSend: function() {
                        $('.loader').show();
                    },
                    success: function(data) {
                        toggleModal('modal-edit-book');
                        form.find('#id').val(data.result.id);
                        form.find('#book_code').val(data.result.book_code);
                        form.find('#title').val(data.result.title);
                        form.find('#year').val(data.result.year);
                        form.find('#author').val(data.result.author);
                        form.find('#stock').val(data.result.stock);
                    },
                    error: function(data) {

                    },
                    complete: function(data) {
                        setTimeout(function() {
                            $('.loader').hide();
                        }, 500);
                    }
                });
            }
        </script>
    @endsection
</x-app-layout>
