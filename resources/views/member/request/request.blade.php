<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Request Books') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">

                <div class="mb-6">
                    <x-primary-button onclick="toggleModal('modal-add-request')" type="button">
                        {{ __('Add Request') }}
                    </x-primary-button>
                </div>

                <table class="table table-striped table-bordered dt-responsive nowrap text-sm"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="tableTransaction">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id</th>
                            <th>Book Code</th>
                            <th>Book Title</th>
                            <th>Status</th>
                            <th>Created At</th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>


    {{-- @include('admin.book.partials.add-book-modal') --}}

    {{-- @include('admin.book.partials.edit-book-modal') --}}

    @include('member.request.partials.add-request-modal')

    @section('script')
        <script type="module">
            // load datatable
            $(function() {
                var table = $('#tableBook').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('book.request.index') }}",
                    pageLength: 10,

                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            sortable: false
                        },
                        {
                            data: 'id',
                            name: 'id',
                            visible: false
                        },
                        {
                            data: 'book_code',
                            name: 'book_code'
                        },
                        {
                            data: 'title',
                            name: 'title'
                        },
                        {
                            data: 'year',
                            name: 'year'
                        },
                        {
                            data: 'author',
                            name: 'author'
                        },
                        {
                            data: 'stock',
                            name: 'stock'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ],
                    columnDefs: [{
                        targets: 6,
                        render: $.fn.dataTable.render.number('.', '.', 0, '')
                    }],
                });
            });


            $(function() {
                var table = $('#tableTransaction').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('member.transaction.index') }}",
                    pageLength: 10,
                    responsive: true,

                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                        },
                        {
                            data: 'id',
                            name: 'id',
                            visible: false
                        },
                        {
                            data: 'book.book_code',
                            name: 'book.book_code',
                            sortable: false
                        },
                        {
                            data: 'book.title',
                            name: 'book.title',
                            sortable: false
                        },
                        {
                            data: 'status',
                            name: 'status',
                            sortable: false
                        },
                        {
                            data: 'created_at',
                            name: 'created_at'
                        },
                        
                    ]
                });
            });

           
        </script>

        <script>
            // select book
            function selectBook(id) {

                let item = {
                    id: id
                }
                var formData = new FormData();

                for (var key in item) {
                    formData.append(key, item[key]);
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "{{ url('book-transaction') }}",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,

                    beforeSend: function() {},
                    success: function(data) {
                        toggleModal('modal-add-request');
                        $('#tableTransaction').DataTable().ajax.reload();
                    },
                    error: function(data) {

                    },
                    complete: function(data) {}
                });
            }
        </script>
    @endsection
</x-app-layout>
