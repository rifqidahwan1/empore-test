<button {{ $attributes->merge(['type' => 'submit', 'class' => 'flex-col min-w-[120px] inline-flex items-center px-4 py-2 bg-green-600 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-green-700 focus:bg-green-700 active:bg-green-700 focus:outline-none transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
