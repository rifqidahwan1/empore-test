<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 text-center font-bold text-gray-900">Online Skill Test PT Empore</div>
                <div class="p-6 text-gray-900">

                    <p>Diperlukan sebuah aplikasi web peminjaman buku sederhana dengan menggunakan
                        framework Laravel yang terdiri dari 2 jenis pengguna, yaitu Admin dan Anggota.
                    </p><br/>
                    <p><b>Berikut merupakan fitur dari admin :</b></p>

                    <ul>
                        <li> - CRUD Master Buku yang memiliki atribut kode buku, judul buku, tahun terbit, penulis
                            dan stok buku.
                           </li>
                           <li> - CRUD Anggota. Admin dapat membuat anggota baru</li>
                        <li> - Pengajuan buku. Untuk melihat list pengajuan peminjaman buku oleh anggota, bisa
                            melakukan approve atau reject. Saat di-approve, otomatis akan mengurangi stok
                            buku.</li>
                        <li> - Peminjaman buku. Bisa melihat list peminjaman buku (pengajuan buku yg diapprove), admin
                            bisa mengubah status peminjaman saat buku sudah dikembalikan
                            oleh anggota sehingga stok buku menjadi bertambah lagi.</li>

                    </ul>


                    <br/>
                    <b>Berikut merupakan fitur dari anggota :</b>
                    <ul>
                        <li> - Anggota bisa melakukan pengajuan peminjaman buku, dengan memilih buku yang
                            ingin dipinjam serta mengisi tanggal peminjaman dan pengembalian.</li>
                        <li> - List peminjaman buku, berisi buku yang telah dan sedang dipinjam oleh anggota
                            tersebut.</li>
                    </ul>

                    <br/>
                    <p><b>Tugas :</b></p>
                    <ul>
                        <li>1. Membuat rancangan database / ERD yang mampu mengakomodasi semua fitur-fitur
                            yang ada di atas.</li><br/>
                        <li> 2. Membuat sebuah aplikasi web dengan fitur yang berwarna merah saja, dengan
                            ketentuan seperti berikut :
                            <ul>
                                <li> - Menggunakan web template admin (misalnya AdminLTE dll)</li>
                                <li> - Multiauth Admin dan Anggota menggunakan Guard</li>
                                <li> - List data ditampilkan berupa DataTables serverside (contoh Yajra datatables)</li>
                                <li> - Menggunakan Eloquent ORM.</li>
                                <li> - Mengimplementasikan jQuery AJAX dalam aplikasi (tidak harus semuanya)</li>
                            </ul>
                        </li><br/>
                        <li>
                            3. Membuat sebuah API sederhana dengan kembalian JSON seperti di bawah:
                            <ul>
                                <li>a. GET /api/books
                                    Mengambil data semua buku
                                </li>
                                <li>b. GET /api/books/{code}
                                    Mengambil data buku sesuai dengan kode
                                </li>
                                <li> c. POST /api/books
                                    Membuat buku baru</li>
                                <li>
                                    d. PUT /api/books/{code}
                                    Mengubah data buku sesuai dengan kode
                                </li>
                                <li>e. DELETE /api/books/{code}
                                    Menghapus data buku sesuai dengan kode</pre>
                                </li>
                            </ul>
                        </li>
                    </ul>





                </div>
            </div>
        </div>
    </div>
</x-app-layout>
