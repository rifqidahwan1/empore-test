<?php

use App\Http\Controllers\TransactionController;
use App\Http\Controllers\MemberTransactionController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BookRequestController;
use App\Http\Controllers\MemberController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');



Route::middleware('admin')->group(function () {
    Route::get('/book', [BookController::class, 'index'])->name('book.index');
    Route::get('/transaction', [TransactionController::class, 'index'])->name('transaction.index');
    Route::post('/transaction', [TransactionController::class, 'update'])->name('transaction.update');
    Route::get('/member/{id?}', [MemberController::class, 'index'])->name('member.index');
    Route::post('/member', [MemberController::class, 'store'])->name('member.create');
    Route::put('/member/{id}', [MemberController::class, 'update'])->name('member.update');
    Route::delete('/member/{id}', [MemberController::class, 'destroy'])->name('member.delete');
});

Route::middleware('member')->group(function () {
    Route::get('/book-request', [BookRequestController::class, 'index'])->name('book.request.index');
    Route::get('/book-transaction', [MemberTransactionController::class, 'index'])->name('member.transaction.index');
    Route::post('/book-transaction', [MemberTransactionController::class, 'create'])->name('member.transaction.create');
});

require __DIR__.'/auth.php';
